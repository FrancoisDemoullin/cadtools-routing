
import queue as Q
import random

import node
import draw
import parser

# returns list of nodes
# each node in returned list represents a reachable, valid tile in the grid
# that can be reached from cur_tile
def get_neighbors(grid, cur_tile, window_width, window_height, index):
	return_me = []
	# can we go up?
	if (cur_tile.y+1 < window_height):
		if (grid[cur_tile.x][cur_tile.y+1] == '' or grid[cur_tile.x][cur_tile.y+1] == str(index)):
			return_me.append(node.node(cur_tile.x, cur_tile.y+1, cur_tile.distance+1, cur_tile)) # yes, we can go up!
	# can we go down?
	if (cur_tile.y-1 >= 0):
		if (grid[cur_tile.x][cur_tile.y-1] == '' or grid[cur_tile.x][cur_tile.y-1] == str(index)):
			return_me.append(node.node(cur_tile.x, cur_tile.y-1, cur_tile.distance+1, cur_tile)) # yes, we can go down!
	# can we go right?
	if (cur_tile.x+1 < window_width):
		if (grid[cur_tile.x+1][cur_tile.y] == '' or grid[cur_tile.x+1][cur_tile.y] == str(index)):
			return_me.append(node.node(cur_tile.x+1, cur_tile.y, cur_tile.distance+1, cur_tile)) # yes, we can go right!
	# can we go left?
	if (cur_tile.x-1 >= 0):
		if (grid[cur_tile.x-1][cur_tile.y] == '' or grid[cur_tile.x-1][cur_tile.y] == str(index)):
			return_me.append(node.node(cur_tile.x-1, cur_tile.y, cur_tile.distance+1, cur_tile)) # yes, we can go left!

	# return possible directions
	return return_me

#
# backtrace method to recreate the path from an annotated grid
#
def backtrace(found_end_points):
	path = []
	for end_point in found_end_points:
		while end_point:
			if not end_point in path: 
				path.append(end_point)
				end_point = end_point.parent
			else:
				break;
	return path

#
# get_path computes the path for cur_sink
# If no path is found, [] is returned, 
# otherwise a list of all consecutive steps for the path is returned
#
def get_path(cur_sink, grid, window_width, window_height, index):

	# set the start point to one of the nodes
	start_point = node.node(cur_sink[0][0], cur_sink[0][1], 0)

	# get a list of all the endpoints
	end_point = []
	for i in range(1, len(cur_sink)):
		end_point.append(node.node(cur_sink[i][0], cur_sink[i][1], 0))

	# maintian a list for all endpoints that have been reached by the path
	found_end_points = []

	# maintain a list for all visited paths
	visited = [start_point]

	# keep track of which nodes to expand using a priority queue
	queue = Q.PriorityQueue()
	queue.put(start_point)

	# while there are nodes to be expanded
	while not queue.empty():

		# get the most promising node
		expand_me = queue.get()

		# debugging purposes
		# print(expand_me.x, expand_me.y, expand_me.distance)

		# expand it
		expand_us = get_neighbors(grid, expand_me, window_width, window_height, index)
		
		# for all the neighbors that can be reached from the current node
		for expandable in expand_us:
			assert(expandable != expand_me)

			# is this expandable a goal node?
			if expandable in end_point:
				# remove it from the goal list, we already found it
				end_point.remove(expandable)
				found_end_points.append(expandable)

			# should we add expandable to the queue?
			if not(expandable in visited):
				queue.put(expandable)
				visited.append(expandable)

			# check if we are done: did we find all the end points
			if not end_point:
				# we found a result, backtrace now
				return backtrace(found_end_points)

	# there was no possible path!
	return []

def draw_found_paths(win, grid, found_paths):
	for cur_path in found_paths:
		path = cur_path[0]
		visualization_index = cur_path[1]
		solution_grid = cur_path[2]
		grid = solution_grid[:]

		draw.draw_path(win, path, visualization_index)
		
#
# High level function to perform lee-more on all paths
# calls find_path for each of the sinks
#
def lee_more(grid, list_of_sinks, window_width, window_height, win):

	# keep a list of found paths for this routing
	found_paths_global = []

	# sink helper correlates a sink to an visualization index and a grid solution
	sink_helper = []
	for index, cur_sink in enumerate(list_of_sinks):
		# note that the index visible in the graphics library should by 1-indexed, not 0 indexed
		sink_helper.append((cur_sink, index+1))

	for iterations in range(5):

		# shuffle the list
		random.shuffle(sink_helper)
		found_paths = []

		# for each potential solution a distinct grid is needed
		temp_grid = grid[:] 

		for cur_sink_helper in sink_helper:
			visualization_index = cur_sink_helper[1]
			cur_sink = cur_sink_helper[0]

			path = get_path(cur_sink, grid, window_width, window_height, visualization_index)
			if path:

				# draw path in the current grid
				for node in path:
					temp_grid[node.x][node.y] = str(visualization_index)

				found_paths.append((path, visualization_index, temp_grid))

		# pick the best path yet as the gloabl_path
		if not found_paths_global:
			found_paths_global = found_paths
		elif len(found_paths) > len(found_paths_global):
			found_paths_global = found_paths

	# path drawing with
	draw.draw_path_animation(win, found_paths_global)

	# path drawing  without animation
	#draw_found_paths(win, grid, found_paths_global)
			

	# debugging purposes only:
	# parser.print_grid(grid, window_width, window_height)