# library imports
from graphics import *
import random
import time

def setup_window(win, window_width, window_height):
	win = GraphWin('Layout', 1024, 1024)
	win.setCoords(0.0, 0.0, window_width, window_height)
	win.setBackground("white")
	return win

def color_square(win, x, y, color):
	square = Rectangle(Point(x,y), Point(x+1,y+1))
	square.draw(win)
	square.setFill(color)
	return win

def number_square(win, x, y, number):
	label = Text(Point(x+0.3, y+0.3), number)
	label.draw(win)
	return win

def draw_obstacles(grid, win, list_of_obstacles):
	for cur_obstacle  in list_of_obstacles:
		grid[int(cur_obstacle[0])][int(cur_obstacle[1])] = 'o'
		color_square(win, int(cur_obstacle[0]), int(cur_obstacle[1]), color_rgb(255, 0, 0));
	return win

def draw_starting_sinks(grid, win, list_of_sinks):
	for i in range(0, len(list_of_sinks)):
		cur_sink = list_of_sinks[i]
		color = color_rgb(random.randint(0,255), random.randint(0,255),
						random.randint(0,255))
		for index, cur_sink_element in enumerate(cur_sink):
				grid[int(cur_sink_element[0])][int(cur_sink_element[1])] = "w 1"
				color_square(win, int(cur_sink_element[0]), int(cur_sink_element[1]), color)
				number_square(win, int(cur_sink_element[0]),
								int(cur_sink_element[1]), i)
		# list_of_sinks[i] = (cur_sink, color) 
	return win

def draw_grid(win, window_height, window_width):
# draw grid
	for x in range(window_height):
		line = Line(Point(0, x), Point(window_width, x))
		line.draw(win)    
	for x in range(window_width):
		line = Line(Point(x, 0), Point(x, window_height))
		line.draw(win)
	return win

def draw_path_animation(win, found_gloabl_paths):
	for cur_path in found_gloabl_paths:
		path = cur_path[0]
		wire_number = cur_path[1] 
		for cur_elem in path:
			number_square(win, cur_elem.x, cur_elem.y, wire_number)
			time.sleep(0.5)
	print("path drawing has ended")


def draw_path(win, path, wire_number):
	for cur_elem in path:
		number_square(win, cur_elem.x, cur_elem.y, wire_number)
	return win
	
