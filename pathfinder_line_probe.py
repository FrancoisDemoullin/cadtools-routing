
import queue as Q
import random
import copy

import line_probe_node as node
import draw
import parser


#
# backtrace method to recreate the path from an annotated grid
#
def backtrace(bt):
	path = []
	cur_node = bt
	while cur_node: 
		path.append(cur_node)
		cur_node = cur_node.parent
	return path

#
# return true or false
# wires can intersect with wires that originate from the same source
# This is not a true intersection
# a true intersection is only found when wires from two distinct source intersect
#
def is_true_intersection(potential_intersection, visited):
	if potential_intersection in visited:
		if visited.index(potential_intersection) >= 0:
			if visited[visited.index(potential_intersection)].master is not potential_intersection.master:
				return 1
	return 0

#
# expand the node!
# draw verticle lines in all directions starting at node expand_me
#
def expand_node(input_grid, expand_me, index, window_width, window_height, visited, master, expanded):
	
	intersection = []
	cur_tile = expand_me

	# expand up	
	while cur_tile.y+1 < window_height and (input_grid[cur_tile.x][cur_tile.y+1] == '' or input_grid[cur_tile.x][cur_tile.y+1] == str(index)):
		# yes, we can go up
		new_node = node.node(cur_tile.x, cur_tile.y+1, 0, cur_tile, master)
		if not new_node in expanded:
			visited.append(new_node)
		if (input_grid[new_node.x][new_node.y] == str(index) and is_true_intersection(new_node, visited)):
			# we found an intersection
			intersection.append(new_node)
		else:
			# mark the grid
			input_grid[new_node.x][new_node.y] = str(index)
		# move to the next node
		cur_tile = new_node

	# expand down	
	cur_tile = expand_me
	while cur_tile.y-1 >= 0 and (input_grid[cur_tile.x][cur_tile.y-1] == '' or input_grid[cur_tile.x][cur_tile.y-1] == str(index)):
		# yes, we can go up
		new_node = node.node(cur_tile.x, cur_tile.y-1, 0, cur_tile, master)
		if not new_node in expanded:
			visited.append(new_node) 
		if (input_grid[new_node.x][new_node.y] == str(index) and is_true_intersection(new_node, visited)):
			# we found an intersection
			intersection.append(new_node)
		else:
			# mark the grid
			input_grid[new_node.x][new_node.y] = str(index)
		# move to the next node
		cur_tile = new_node

	cur_tile = expand_me

	# expand right	
	while cur_tile.x+1 < window_width and (input_grid[cur_tile.x+1][cur_tile.y] == '' or input_grid[cur_tile.x+1][cur_tile.y] == str(index)):
		# yes, we can go up
		new_node = node.node(cur_tile.x+1, cur_tile.y, 0, cur_tile, master)
		if not new_node in expanded:
			visited.append(new_node) 
		if (input_grid[new_node.x][new_node.y] == str(index) and is_true_intersection(new_node, visited)):
			# we found an intersection
			intersection.append(new_node)
		else:
			# mark the grid
			input_grid[new_node.x][new_node.y] = str(index)
		# move to the next node
		cur_tile = new_node

	cur_tile = expand_me

	# expand left	
	while cur_tile.x-1 >= 0 and (input_grid[cur_tile.x-1][cur_tile.y] == '' or input_grid[cur_tile.x-1][cur_tile.y] == str(index)):
		# yes, we can go up
		new_node = node.node(cur_tile.x-1, cur_tile.y, 0, cur_tile, master)
		if not new_node in expanded:
			visited.append(new_node)
		if (input_grid[new_node.x][new_node.y] == str(index) and is_true_intersection(new_node, visited)):
			# we found an intersection
			intersection.append(new_node)
		else:
			# mark the grid
			input_grid[new_node.x][new_node.y] = str(index) 
		# move to the next node
		cur_tile = new_node

	return (visited, intersection, input_grid)

#
# get_path computes the path for cur_sink
# If no path is found, [] is returned, 
# otherwise a list of all consecutive steps for the path is returned
#
def get_path(cur_sink, input_grid, window_width, window_height, index):

	temp_grid = input_grid

	path = []

	# get a list of all the points that need expansion
	expand_us = []
	
	for i in range(0, len(cur_sink)):
		expand_us.append(node.node(cur_sink[i][0], cur_sink[i][1], 0))

	sink_number = len(expand_us)
	expanded = []
	intersection = []
	visited = []

	found_goals = []

	while expand_us:
		if expand_us[0] and expand_us[0] not in found_goals:
			if not expand_us[0].master: 
				expand_results = expand_node(temp_grid, expand_us[0], index, window_width, window_height, visited, expand_us[0], expanded)
			else:
				expand_results = expand_node(temp_grid, expand_us[0], index, window_width, window_height, visited, expand_us[0].master, expanded)
			expanded.append(expand_us[0])
			intersection.extend(expand_results[1])
			temp_grid = expand_results[2]
			visited.extend(expand_results[0])
		del expand_us[0]

		if intersection:
			# print(intersection)
			for intersect in intersection:
				assert(is_true_intersection(intersect, visited))
				
				other_end_of_intersection = visited[visited.index(intersect)]

				if intersect.master in found_goals and other_end_of_intersection.master in found_goals:
					# these two sinks have been connected already
					continue

				if not found_goals or not other_end_of_intersection in path:
					# back trace to both sides of the intersection to find a path
					# this is the first intersection
					path.extend(backtrace(intersect))
					found_goals.append(intersect.master)

					# trace the other end of intersection
					path.extend(backtrace(other_end_of_intersection))
					found_goals.append(other_end_of_intersection.master)

				# connect to already existing piece of the path
				if found_goals and not intersect.master in found_goals and other_end_of_intersection in path:
					path.extend(backtrace(intersect))
					found_goals.append(intersect.master)

				if len(found_goals) == sink_number:
					# the full path was found!
					print("full path was found")
					print(len(expanded))
					parser.print_grid(input_grid, window_width, window_height)
					return path

		expand_us.extend(visited)
	
	# only a partial path was found
	return path		

def draw_found_paths(win, grid, found_paths):
	for cur_path in found_paths:
		path = cur_path[0]
		visualization_index = cur_path[1]
		solution_grid = cur_path[2]
		grid = solution_grid[:]

		draw.draw_path(win, path, visualization_index)
		
#
# High level function to perform line-probe on all paths
# calls find_path for each of the sinks
#
def line_probe(grid, list_of_sinks, window_width, window_height, win):

	# keep a list of found paths for this routing
	found_paths_global = []

	# sink helper correlates a sink to an visualization index and a grid solution
	sink_helper = []
	for index, cur_sink in enumerate(list_of_sinks):
		# note that the index visible in the graphics library should by 1-indexed, not 0 indexed
		sink_helper.append((cur_sink, index+1))

	for sink in sink_helper:
		clean_grid = copy.deepcopy(grid)
		path = get_path(sink[0], clean_grid, window_width, window_height, sink[1])

		if path: 
			found_paths_global.append((path, sink[1]))

	# path drawing animaiton
	draw.draw_path_animation(win, found_paths_global)

	# if you want to visualize the final result only: uncomment the following line:
	#for path in found_paths_global:
		#draw.draw_path(win, path[0], path[1])
				
	# debugging purposes only:
	# parser.print_grid(grid, window_width, window_height)