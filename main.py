
# library imports
from graphics import *
import argparse

# my imports
import parser
import draw
import node
import pathfinder
import pathfinder_line_probe

def dist(a, b):
	return abs(a.x - b.x) + abs(a.y - b.y)

def main(file_name, line_probe):
		
		# state variables
		window_height = 0
		window_width = 0
		window_size_tuple = [window_height, window_width]

		list_of_obstacles = []
		list_of_sinks = []

		# graphics 
		win = 0

		# parse the file
		window_size_tuple = parser.parse_file(file_name, window_height, window_width, list_of_obstacles, list_of_sinks)
		window_width = window_size_tuple[0]
		window_height = window_size_tuple[1]

		# error checking
		assert(window_height)
		assert(window_width)
		assert(list_of_obstacles)
		assert(list_of_sinks)

		# create internal grid
		grid = [[0 for x in range(window_height)] for y in
						range(window_width)]
		for x in range(window_width):
			for y in range(window_height):
				grid[x][y] = ''
		
		# setup internal grid representation
		parser.add_obstacles(grid, list_of_obstacles)
		parser.add_sinks(grid, list_of_sinks)

		# debugging purposes only
		# parser.print_grid(grid, window_width, window_height)

		# graphics - setup the window
		win = draw.setup_window(win, window_width, window_height)
		win = draw.draw_grid(win, window_height, window_width)
		win = draw.draw_obstacles(grid, win, list_of_obstacles)
		# win = draw.draw_starting_sinks(grid, win, list_of_sinks)

		# get the paths
		if not line_probe:
			pathfinder.lee_more(grid, list_of_sinks, window_width, window_height, win)
		else:
			pathfinder_line_probe.line_probe(grid, list_of_sinks, window_width, window_height, win)

		# Let the user know that we are done:
		print("rendering is done! All found paths have been displayed.")

		# this will leave the window open until the user clicks
		win.getMouse()
		win.close()


# command line parser
cmd_parser = argparse.ArgumentParser(description='Process some integers.')
cmd_parser.add_argument('filename', metavar='filename', type=str, nargs='+', help='')
cmd_parser.add_argument('--line_probe', dest='line_probe', type=int)
args = cmd_parser.parse_args()

# call the main function using the parsed commands
main(args.filename[0], args.line_probe)
