from functools import total_ordering

# node class

# each point in the grid is a node, this is used by A* for exploring the search space
# the parent node is used for backtracing through the annotated path
@total_ordering
class node:
	def __init__(self, x, y, distance, parent=None):
		self.parent = parent 
		self.x = x
		self.y = y
		self.distance = distance 

	def __eq__(self, other):
		return (self.x == other.x and self.y == other.y)
	def __lt__(self, other):
		return (self.distance < other.distance)