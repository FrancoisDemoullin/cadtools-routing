
#
# Parses the input file
#
# Fill in the window_height, window_width and list_of_obstacles for a given
# infile
#
def parse_file(filename, window_height, window_width, list_of_obstacles, list_of_sinks):
    # open the file    
    inputfile = open(filename)
    
    # parse the first line - the window size
    first_line = inputfile.readline()
    first_line = first_line.split()
    window_width = int(first_line[0])
    window_height = int(first_line[1])
    
    # parse the obstacles and store them in the list_of_obstacles array
    num_obstacles = int(inputfile.readline().split()[0])
    for i in range(num_obstacles):
        cur_line = inputfile.readline().split()
        list_of_obstacles.append(cur_line)
    
    # parse the sinks
    num_sinks = int(inputfile.readline().split()[0])
    for i in range(num_sinks):
        cur_sink = inputfile.readline().split()
        append_me = []
        for j in range(0, int(cur_sink[0])):
            helper = (int(cur_sink[2*j+1]), int(cur_sink[2*j+2]))
            append_me.append(helper) 
        list_of_sinks.append(append_me)
    
    return [window_width, window_height]

#
# These functions maintain an internal representation of the grid
# The internal grid is 2d array accessible, like so: grid[x][y]
#
def add_obstacles(grid, list_of_obstacles):
    for cur_obstacle  in list_of_obstacles:
        grid[int(cur_obstacle[0])][int(cur_obstacle[1])] = 'o'  

def add_sinks(grid, list_of_sinks):
    for i in range(0, len(list_of_sinks)):
        cur_sink = list_of_sinks[i]
        for cur_sink_element in cur_sink:
                grid[int(cur_sink_element[0])][int(cur_sink_element[1])] = str(i+1)

def print_grid(grid, window_width, window_height):
        for i in range(window_height-1, -1, -1):
                for j in range(0, window_width):
                        print(grid[j][i], end=' ')
                        #print()
                print()
        print('***************************')
